       IDENTIFICATION DIVISION.
       PROGRAM-ID. SUPPLIER-ENTRY.
       AUTHOR. SEMPIO, JETHRO KYLE B.
       DATE-WRITTEN. MARCH 12, 2018
       DATE-COMPILED.

       ENVIRONMENT DIVISION.
       
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT SUP-FILE
             ASSIGN TO DISK 
             ORGANIZATION IS INDEXED
             ACCESS MODE IS DYNAMIC
             RECORD KEY IS SUP-ID
             FILE STATUS IS SUP-STAT.

       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-PC.
       OBJECT-COMPUTER. IBM-PC.

       DATA DIVISION.
       FILE SECTION.
       FD SUP-FILE
              LABEL RECORDS ARE STANDARD
              VALUE OF FILE-ID IS 'SUP.DAT'.

       01 SUP-RECORD.
           02 SUP-ID         PIC X(5).
           02 SUP-NAME       PIC X(20).
           02 SUP-REP		     PIC X(20).
           02 SUP-PHONE		   PIC X(11).
           02 SUP-EMAIL		   PIC X(20).
           02 SUP-ADDR.
              05 ADDR-NUM          PIC X(5).
              05 ADDR-STREET       PIC X(20).
              05 ADDR-BRGY         PIC X(15).
              05 ADDR-CITY         PIC X(15).
              05 ADDR-PROV         PIC X(15).
              05 ADDR-COUNTRY      PIC X(15).
              05 ADDR-ZIP          PIC 9(4).

       WORKING-STORAGE SECTION.
       01 WS-SUP-RECORD.
           02 WS-SUP-ID        PIC X(5).
           02 WS-SUP-NAME      PIC X(20).
           02 WS-SUP-REP		   PIC X(20).
           02 WS-SUP-PHONE		 PIC X(11).
           02 WS-SUP-EMAIL		 PIC X(20).
           02 WS-SUP-ADDR.
              05 WS-ADDR-NUM          PIC X(5).
              05 WS-ADDR-STREET       PIC X(20).
              05 WS-ADDR-BRGY         PIC X(15).
              05 WS-ADDR-CITY         PIC X(15).
              05 WS-ADDR-PROV         PIC X(15).
              05 WS-ADDR-COUNTRY      PIC X(15).
              05 WS-ADDR-ZIP          PIC 9(4).
       77 SUP-STAT                 PIC XX.
       77 ISFOUND                  PIC 9.
       77 OPT                      PIC X.
       77 ISLOOP                   PIC 9 VALUE 1.


       SCREEN SECTION.
       01 ENTER-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "쿟-SHIRT PRINTING INVT.   �".
           02 LINE 12 COLUMN 30 "쿞UPPLIER ENTRY           �".
           02 LINE 13 COLUMN 30 "�                         �".
           02 LINE 14 COLUMN 30 "�                         �".
           02 LINE 15 COLUMN 30 "�     ENTER SUP-ID        �".
           02 LINE 16 COLUMN 30 "�         *****           �".
           02 LINE 17 COLUMN 30 "�         컴컴�           �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "�                         �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".

       01 DATA1-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "SUP ID:                1/2�".
           02 LINE 12 COLUMN 30 "쳐컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 13 COLUMN 30 "쿞UPPLIER NAME:           �".
           02 LINE 14 COLUMN 30 "�                         �".
           02 LINE 15 COLUMN 30 "쿝EPRESENTATIVE:          �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "쿛HONE:                   �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "쿐MAIL:                   �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".
       
       01 DATA2-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "쿌DDRESS               2/2�".
           02 LINE 12 COLUMN 30 "쳐컴컴컴컴컴컫컴컴컴컴컴컴�".
           02 LINE 13 COLUMN 30 "쿙UMBER:     쿎OUNTRY:    �".
           02 LINE 14 COLUMN 30 "�            �            �".
           02 LINE 15 COLUMN 30 "쿞TREET:     쿩IP:        �".
           02 LINE 16 COLUMN 30 "�            �            �".
           02 LINE 17 COLUMN 30 "쿍RGY:       �            �".
           02 LINE 18 COLUMN 30 "�            �            �".
           02 LINE 19 COLUMN 30 "쿎ITY:       �            �".
           02 LINE 20 COLUMN 30 "�            �            �".
           02 LINE 21 COLUMN 30 "쿛ROV:       �            �".
           02 LINE 22 COLUMN 30 "�            �            �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컨컴컴컴컴컴컴�".

       01 CONFIRM-SAVE-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "�                         �".
           02 LINE 12 COLUMN 30 "�                         �".
           02 LINE 13 COLUMN 30 "�                         �".
           02 LINE 14 COLUMN 30 "�          SAVE?          �".
           02 LINE 15 COLUMN 30 "�         ( Y/N )         �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "�                         �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "�                         �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".

       01 REQUERY-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "�                         �".
           02 LINE 12 COLUMN 30 "�                         �".
           02 LINE 13 COLUMN 30 "�                         �".
           02 LINE 14 COLUMN 30 "�  QUERY SUPPLIER AGAIN?  �".
           02 LINE 15 COLUMN 30 "�          (Y/N)          �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "�                         �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "�                         �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".
       
       01 X-SCREEN.
           02 LINE 10 COLUMN 30 "旼컴컴컴컴컴컴컴컴컴컴컴컴�".
           02 LINE 11 COLUMN 30 "�                         �".
           02 LINE 12 COLUMN 30 "�                         �".
           02 LINE 13 COLUMN 30 "�                         �".
           02 LINE 14 COLUMN 30 "�                         �".
           02 LINE 15 COLUMN 30 "�                         �".
           02 LINE 16 COLUMN 30 "�                         �".
           02 LINE 17 COLUMN 30 "�                         �".
           02 LINE 18 COLUMN 30 "�                         �".
           02 LINE 19 COLUMN 30 "�                         �".
           02 LINE 20 COLUMN 30 "�                         �".
           02 LINE 21 COLUMN 30 "�                         �".
           02 LINE 22 COLUMN 30 "�                         �".
           02 LINE 23 COLUMN 30 "읕컴컴컴컴컴컴컴컴컴컴컴컴�".

       01 CLEAR-SCREEN.
           05 BLANK SCREEN.

       PROCEDURE DIVISION.
       MAIN-PROGRAM.
           PERFORM ENTER-SUP-ID UNTIL ISLOOP = 0.
           DISPLAY X-SCREEN.
           STOP RUN.

       OPEN-FILE.
           OPEN I-O SUP-FILE.
           IF SUP-STAT NOT = '00'
              OPEN OUTPUT SUP-FILE
              CLOSE SUP-FILE
              OPEN I-O SUP-FILE.

       ENTER-SUP-ID.
           PERFORM CLEAR-SUP-FIELDS.
           DISPLAY CLEAR-SCREEN.
           DISPLAY ENTER-SCREEN.
           ACCEPT (16, 40) WS-SUP-ID.
           PERFORM SEARCH-SUPPLIER.    
            
       SEARCH-SUPPLIER.
           PERFORM OPEN-FILE.
           MOVE WS-SUP-ID TO SUP-ID.
           MOVE 1 TO ISFOUND.
           READ SUP-FILE RECORD
           INVALID KEY
           MOVE 0 TO ISFOUND.
              
           IF ISFOUND = 0
              PERFORM ENTER-SUPPLIER
           ELSE IF ISFOUND = 1
              PERFORM SHOW-SUPPLIER.
           CLOSE SUP-FILE.
           DISPLAY CLEAR-SCREEN.
           DISPLAY REQUERY-SCREEN.
           MOVE SPACES TO OPT.
           ACCEPT (22, 31) OPT.
           IF OPT = 'Y' OR OPT = 'y'
              MOVE 1 TO ISLOOP
           ELSE
              MOVE 0 TO ISLOOP.

       SAVE-SUPPLIER.
           MOVE WS-SUP-ID       TO SUP-ID.
           MOVE WS-SUP-NAME     TO SUP-NAME.
           MOVE WS-SUP-REP      TO SUP-REP.
           MOVE WS-SUP-PHONE    TO SUP-PHONE.
           MOVE WS-SUP-EMAIL    TO SUP-EMAIL.
           MOVE WS-ADDR-NUM     TO ADDR-NUM.
           MOVE WS-ADDR-STREET  TO ADDR-STREET.
           MOVE WS-ADDR-BRGY    TO ADDR-BRGY.
           MOVE WS-ADDR-CITY    TO ADDR-CITY.
           MOVE WS-ADDR-PROV    TO ADDR-PROV.
           MOVE WS-ADDR-COUNTRY TO ADDR-COUNTRY.
           MOVE WS-ADDR-ZIP     TO ADDR-ZIP.
           WRITE SUP-RECORD.

       CLEAR-SUP-FIELDS.
           MOVE SPACES TO SUP-ID.
           MOVE SPACES TO SUP-NAME.
           MOVE SPACES TO SUP-REP.
           MOVE SPACES TO SUP-PHONE.
           MOVE SPACES TO SUP-EMAIL.
           MOVE SPACES TO ADDR-NUM.
           MOVE SPACES TO ADDR-STREET.
           MOVE SPACES TO ADDR-BRGY.
           MOVE SPACES TO ADDR-CITY.
           MOVE SPACES TO ADDR-PROV.
           MOVE SPACES TO ADDR-COUNTRY.
           MOVE ZEROES TO ADDR-ZIP.
       
       SHOW-SUPPLIER.
           DISPLAY DATA1-SCREEN.
           DISPLAY (1, 1) "NEXT PAGE (ENTER)".

           DISPLAY (11, 39) SUP-ID.
           DISPLAY (14, 31) SUP-NAME.
           DISPLAY (16, 31) SUP-REP.
           DISPLAY (18, 31) SUP-PHONE.
           DISPLAY (20, 31) SUP-EMAIL.
           ACCEPT (22, 48) OPT.
           
           DISPLAY CLEAR-SCREEN.

           DISPLAY DATA2-SCREEN.
           DISPLAY (1, 1) "NEXT PAGE (ENTER)".

           DISPLAY (14, 31) ADDR-NUM.
           DISPLAY (16, 31) ADDR-STREET.
           DISPLAY (18, 31) ADDR-BRGY.
           DISPLAY (20, 31) ADDR-CITY.
           DISPLAY (22, 31) ADDR-PROV.
           DISPLAY (14, 44) ADDR-COUNTRY.
           DISPLAY (16, 44) ADDR-ZIP.
           ACCEPT (22, 48) OPT.      

       ENTER-SUPPLIER.
           DISPLAY CLEAR-SCREEN.
           DISPLAY DATA1-SCREEN.
           DISPLAY (11, 39) WS-SUP-ID.
           ACCEPT (14, 31) WS-SUP-NAME.
           ACCEPT (16, 31) WS-SUP-REP.
           ACCEPT (18, 31) WS-SUP-PHONE.
           ACCEPT (20, 31) WS-SUP-EMAIL.

           DISPLAY CLEAR-SCREEN.
           DISPLAY DATA2-SCREEN.
           ACCEPT (14, 31) WS-ADDR-NUM.
           ACCEPT (16, 31) WS-ADDR-STREET.
           ACCEPT (18, 31) WS-ADDR-BRGY.
           ACCEPT (20, 31) WS-ADDR-CITY.
           ACCEPT (22, 31) WS-ADDR-PROV.
           ACCEPT (14, 44) WS-ADDR-COUNTRY.
           ACCEPT (16, 44) WS-ADDR-ZIP.

           DISPLAY CLEAR-SCREEN.
           DISPLAY CONFIRM-SAVE-SCREEN.
           MOVE SPACES TO OPT.
           ACCEPT (22, 31) OPT.
           IF OPT = 'Y' OR OPT = 'y'
              PERFORM SAVE-SUPPLIER.